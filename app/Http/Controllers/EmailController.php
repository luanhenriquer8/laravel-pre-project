<?php
/**
 * Created by PhpStorm.
 * User: luanhenriquer8
 * Date: 04/08/18
 * Time: 11:08
 */

namespace App\Http\Controllers;


class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal-content/email');
    }
}